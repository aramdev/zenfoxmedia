import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import users from './modules/users';
import todo from './modules/todo';

export default new Vuex.Store({
  modules: {
    users,
    todo
  },
  strict: process.env.NODE_ENV !== 'production'
})
