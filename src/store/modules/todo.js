export default {
    namespaced: true,
    state: {
        todo: null,
        user: null
    },
    getters: {
        loaded: (state) => state.todo !== null,
        list: (state) => state.todo,
        user: (state) => state.user,
    },
    mutations: {
        set(state, data){
            state.todo = data
        },
        setUser(state, data){
            state.user = data
        },
        clear(state){
            state.todo = null
        }
    },
    actions: {
        async load(store, id){
            try {
                let response =  await fetch(`https://jsonplaceholder.typicode.com/todos?userId=${id}`, {
                    header: 'Access-Control-Allow-Origin'
                })
                let data = await response.json()
                store.commit('set', data);
            }catch (e){
               console.log(e)
               throw e
            }
        },
        async loadUser(store, id){
            try {
                let response =  await fetch(`http://jsonplaceholder.typicode.com/users/${id}`, {
                    header: 'Access-Control-Allow-Origin'
                })
                let data = await response.json()
                store.commit('setUser', data);
                
            }catch (e){
               console.log(e)
               throw e
            }
        },
        clear(store){
            store.commit('clear');
        }
    }
}



