export default {
    namespaced: true,
    state: {
        users: null,
        count: 0
    },
    getters: {
        loaded: state => state.users !== null,
        list: state => state.users,
        count: state => parseInt(state.count),
    },
    mutations: {
        set(state, data){
            state.users = data
        },
        setCount(state, data){
            state.count = data
        },
        clear(state){
           state.users = null
        },
        clearCount(state){
            state.count = 0
        },

    },
    actions: {
        async load(store, page){
            try{
                let response =  await fetch(`http://jsonplaceholder.typicode.com/users?_page=${page}&_limit=5`,{
                    header: 'Access-Control-Allow-Origin'
                })
                let count = parseInt(response.headers.get('x-total-count'))
                store.commit('setCount', count);
                let data = await response.json()
                store.commit('set', data);
            }catch (e){
                console.log(e)
                throw e
            }
        },
        clear(store){
            store.commit('clear');
            store.commit('clearCount');
        }
    }
}


