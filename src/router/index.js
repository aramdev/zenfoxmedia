import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '~/store'

Vue.use(VueRouter)


const routes = [
  {
    name: 'home',
    path: '',
    redirect: '/users/page/1'
  },
  {
    path: '/users/page/:id',
    name: 'users',
    component: () => import('@/views/Users.vue')
  },
  {
    path: '/todo/:id',
    name: 'todo',
    component: () => import('@/views/Todo.vue'),
  },
  {
    path: '*',
    component: () => import('@/views/E404.vue'),
  }
  
]

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

export default router
